package INF101.lab2;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.ArrayList;


public class Fridge implements IFridge {
	
	private List<FridgeItem> items = new ArrayList<FridgeItem>();
	private int size = 20;

	@Override
	public int nItemsInFridge() {
		return items.size();
	}

	@Override
	public int totalSize() {
		return size;
	}

	@Override
	public boolean placeIn(FridgeItem item) {
		if (items.size() == size) return false;
		return items.add(item);
	}

	@Override
	public void takeOut(FridgeItem item) {
		boolean removed = items.remove(item);
		if (removed == false) {
			throw new NoSuchElementException();
		}
	}

	@Override
	public void emptyFridge() {
		items.clear();
	}

	@Override
	public List<FridgeItem> removeExpiredFood() {
		List<FridgeItem> ExpiredFood = new ArrayList<>();
		
		for (FridgeItem item : items) {
			if (item.hasExpired()) {
				ExpiredFood.add(item);
			}
		}
		
		for (FridgeItem item : ExpiredFood) {
			takeOut(item);
		}
		
		return ExpiredFood;
	}
	
}
